# CTF-TOOL
A tool for building CTFs with minimum effort.

User guide documentation is available at https://cyberatuc.gitlab.io/ctf-tool

## What is it?
ctf-tool is similar to other CTF configuration and deployment tools, it has a
stronger emphasis on ease of use and quick time to deployment. ctf-tool has
been used to run over 10 CTF events at the [University of Cincinnati][0] since
its creation at/for the campus hackathon in 2018.

## Features
ctf-tool is currently capable of running small to medium scale CTFs of 10-100 players
without any additional configuration. Challenges can be services or just files that
are given to the players. Challenges are also dynamically built per deployment, so
they can be randomized and re-used without fear of players hoarding flags or exact
solutions between events.

ctf-tool does not only focus on the deployment of challenges, but also aims to provide
insight into the challenges that are being configured, including authorship attribution,
technical summaries, and visualizations of how balanced (or unbalanced) the end event
may seem to players.

## Challenge Packs
ctf-tool's design revolves around the concept of challenge packs which are simple
directories of challenges. The reasoning of using challenge packs as a building
block is that it allows problems to be neatly sorted, reused, and allows the CTF
builder to adjust the difficulty for the audience's skill level by combining
different challenge packs into a single build.

## Flags
For the sake of simplicity, ctf-tool currently only supports a single static
flag per problem. Flags should look something like `ctf{flag}` where `ctf` is a
slug for your event and `flag` is your challenge's specific flag. Randomizing
flags at build time is also an option.

CTFd supports multiple flags and regex flags which is a planned feature for
ctf-tool's future.

## CTF Scoreboard / Challenge Host Machines:
The CTF Scoreboard is the service that the players will be directed to in
order to view challenges and submit flags for points. In the current
version, ctf-tool expects the CTF host to be an instance of CTFd, in future
versions other scoreboards may be supported as well. We have had a large
amount of success using the CTFd docker image. see
[scripts/ctfd-docker.sh](scripts/ctfd-docker.sh)

The Challenge Host is the machine that ctf-tool expects to run services on
for players to be able to connect to. In current versions, this means a docker host.
The challenge host will also be public facing and presumably under active exploitation
form players. This means that the Challenge Host should be a non-critical machine
can be thrown away when the event is over.

ctf-tool considers the challenge host and the ctf host to be two separate machines
although this is not required in any way. Using one machine for both should be
perfectly fine. The first time ctf-tool was actually used for a ctf, only one machine was used.

## Installation
See [the documentation.][1]

<!-- References -->
[0]: uc.edu
[1]: https://cyberatuc.gitlab.io/ctf-tool/installation/installing-ctf-tool.html#from-gitlab
