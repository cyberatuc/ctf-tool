import argparse
import os
import shutil
import logging
from pathlib import Path
from typing import List

from yaml import dump
from jinja2 import Template

from ctf_tool.challenge import Challenge
from ctf_tool.util import contents_of
from ctf_tool.util import get_resource_file_path
from ctf_tool.tor import tor_compose_container, generate_torrc


logger = logging.getLogger(__file__)


docker_readme_message = \
"""
Docker compose environments are now per-challenge.

The top level docker-compose file is maintained as a single target for building
and pushing challenge images to registries. However, it does not contain port
mapping information as it is not meant to run the challenge services.

The tor service forwarder, if enabled at build time, is also in its own compose
file now. Be aware that this uses forwarding to host machine ports so if it is
left open and a new service starts at the same port as an old one, that service
wil be tor-routable.
"""


def docker_compose_template():
    return {
        'version': '3.3',
        'services': {}
    }


class DockerComposeInfrastructure:
    """
    Module for docker-compose service infrastructure.

    Works by copying each built challenge that requires
    a service. If the service does not have a Dockerfile,
    a basic one will be copied and configured for the challenge.
    """
    def __init__(self, event, args):
        self.event = event
        self.should_run = args['docker']
        self.tor = args['docker_tor']
        self.image_registry = args['docker_registry']
        self.image_tag = args['docker_image_tag']
        self.image_name_pattern = args['docker_image_name']

        if self.tor and not self.should_run:
            raise RuntimeError("--docker-tor cannot be used without --docker")

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--docker","--install-docker",
                            dest="docker",
                            default=True,
                            action='store_true',
                            help="Install service challenges through docker")
        parser.add_argument("--docker-registry",
                            help="Docker registry to use",
                            default='library')
        parser.add_argument("--docker-image-tag",
                            help="Tag to use with all built containers",
                            default="latest")
        parser.add_argument("--docker-image-name",
                            help="Python f-string pattern to use for naming docker images",
                            default="{docker.image_registry}/{event.name}/{challenge.category}/{challenge.username}/{service}:{docker.image_tag}")
        parser.add_argument("--docker-tor",
                            action='store_true',
                            default=False,
                            help="Expose challenges via TOR network")

    def generate(self):
        if not self.should_run:
            logger.debug("--docker not given, skipping docker infrastructure module")
            return

        # Identify challenges needing docker and output compose
        challenges_requiring_server = [
            challenge for challenge in self.event.challenges
            if challenge.requires_server_path or len(challenge.services)]

        self._create_challenge_docker_compose(challenges_requiring_server)

        logger.info("Built docker environment")

    def _create_challenge_docker_compose(self, challenges: List[Challenge]):
        """Given a list of challenges, output the docker-compose environment"""
        # Make output dir
        dockerenv_path = os.path.join(self.event.out_dir, "dockerenv")
        os.makedirs(dockerenv_path)
        with open(Path(dockerenv_path) / "docker-compose.readme", 'w') as fp:
            fp.write(docker_readme_message)

        # Constants
        # TODO: install_required_packages bad
        listener_script_path = get_resource_file_path("challenge-listener.py")
        required_package_script_path = get_resource_file_path("install_required_packages.sh")

        global_compose_dct = docker_compose_template()

        # Make docker-compose file and per-challenge dir/service
        for challenge in challenges:
            logger.debug(f"docker: generating for {challenge.name}")
            docker_compose_dct = docker_compose_template()

            # append `nc server port` to service challenge descriptions
            _connection_hint = Template(challenge.connection_hint)
            _connection_hint = _connection_hint.render({
                "challenge": challenge,
                "event": self.event
            })
            challenge.connection_info = _connection_hint

            # copy per-challenge dir
            challenge_docker_path = os.path.join(dockerenv_path, challenge.username)
            shutil.copytree(challenge.directory, challenge_docker_path)

            # copy infrastructure template files
            shutil.copy2(listener_script_path, challenge_docker_path)
            shutil.copy2(required_package_script_path, challenge_docker_path)

            # service image names / dockerfiles
            for name, config in challenge.services.items():
                # skip named images
                if config.get('image', False):
                    continue
                logger.debug(f"docker: {challenge.name}/{name}")

                # generate image name according to our pattern rules
                image_name = self.image_name_pattern.format(
                    event = self.event,
                    challenge = challenge,
                    docker=self,
                    service=name
                ).lower()

                config['image'] = image_name

                # render dockerfile
                dockerfile_path = (
                    Path(challenge_docker_path) /
                    config['build'].get('context', '.') /
                    config['build'].get('dockerfile', 'Dockerfile')
                )
                render_dockerfile(dockerfile_path, challenge)

            # challenge compose
            docker_compose_dct['services'].update(challenge.services)

            # top-level build file
            for service in challenge.services:
                if not challenge.services[service].get('build', False):
                    continue
                context = challenge.services[service]['build']['context']
                context = str(Path(challenge.username) / context)
                challenge.services[service]['build']['context'] = context
                global_compose_dct['services'][challenge.username + '_' + service] = \
                    challenge.services[service]

            # Write per-challenge docker-compose.yml
            docker_compose_path = os.path.join(challenge_docker_path, "docker-compose.yml")
            with open(docker_compose_path, "w") as f:
                f.write(dump(docker_compose_dct))

        # Write build-only docker-compose.yml
        docker_compose_path = os.path.join(dockerenv_path, "docker-compose.yml")
        with open(docker_compose_path, "w") as f:
            f.write(dump(global_compose_dct))

        # Tor service router
        if self.tor:
            torrc_file_name = os.path.join(self.event.out_dir, "dockerenv", "torrc")
            with open(torrc_file_name, "w") as fp:
                fp.write(generate_torrc(self.event.name, challenges))
            os.mkdir(os.path.join(dockerenv_path, "tor-data"))
            os.chmod(os.path.join(dockerenv_path, "tor-data"), 0o777)
            docker_compose_dct = docker_compose_template()
            docker_compose_dct['services']['tor'] = tor_compose_container
            docker_compose_path = os.path.join(dockerenv_path, "docker-compose.tor.yml")
            with open(docker_compose_path, "w") as f:
                f.write(dump(docker_compose_dct))


def render_dockerfile(out_path, challenge):
    if os.path.exists(out_path):
        with open(out_path) as fp:
            template = Template(fp.read())
    else:
        # TODO: challenges should be required to bring their own Dockerfile
        template = contents_of(get_resource_file_path("Dockerfile"))
        template = Template(template)

    result = template.render({
        'challenge': challenge
    })
    with open(out_path, "w") as fp:
        fp.write(result)
