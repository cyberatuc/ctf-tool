# CTFd utility functions
# TODO: add json-schema validation so we don't have to go through the RE nightmare again
import os
import time
from typing import List
import logging
import json
import shutil
import tempfile
from zipfile import ZipFile
import zipfile
import pathlib
import uuid
import argparse

from ctf_tool.util import get_resource_file_path
from ctf_tool.challenge import Challenge

logger = logging.getLogger(__file__)


class CTFdScoreboard:
    def __init__(self, event, args):
        self.event = event
        self.should_run = args['ctfd']
        self.basezip = args['ctfd_basezip']

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--ctfd",
                            # help="build a backup that can be imported into CTFd",
                            help=argparse.SUPPRESS,
                            action="store_true",
                            default=True)
        parser.add_argument("--ctfd-basezip","--basezip",
                            help="Zip file to pull ctfd metadata from, use a fresh CTFd instance export if you need one",
                            default=get_resource_file_path("ctfd.base.zip"))

    def generate(self):
        if not self.should_run:
            logger.debug("--ctfd not given, skipping ctfd scoreboard module")
            return

        logger.info("Generating CTFd scoreboard artifacts")

        # Build file list (need to copy our files to the temp dir soon too)
        tempdirname, tempuploaddir, tempdbdir = self.make_output_folder()
        logger.info("Created output directories")

        # Build the flag list
        challenge_flag_list = get_ctfd_flag_list(self.event.challenges)

        # Copy challenge files into our temp dir
        for chal in self.event.challenges:
            copy_zip_file_to_temp(chal, tempuploaddir)
        logger.info("Created CTFd uploads")

        # Create CTFd database objects
        # Assign unique ID's to challenges
        challenge_file_list = []
        for file_list in [ctfd_file_list(chal) for chal in self.event.challenges]:
            challenge_file_list += file_list
        challenge_file_list = [file for file in challenge_file_list if file is not None]
        for i in range(len(challenge_file_list)):
            challenge_file_list[i].id = i + 1

        # Output CTFd jsons
        with open(f"{tempdbdir}/challenges.json", "w") as chal_json:
            _json_repr_challenges = [ctfd_repr(chal) for chal in self.event.challenges]
            json.dump(dump_to_json(_json_repr_challenges), chal_json)
            logger.info("Created CTFd challenges table")

        with open(f"{tempdbdir}/files.json", "w") as files_json:
            json.dump(dump_to_json(challenge_file_list), files_json)
            logger.info("Created CTFd files table")

        with open(f"{tempdbdir}/flags.json", "w") as flags_json:
            json.dump(dump_to_json(challenge_flag_list), flags_json)
            logger.info("Created CTFd flags table")

        # Make CTFd config zip
        # unzip existing CTFd meta (we'll use every file that we didn't generate a version of)
        temp_unzip_dir = os.path.join(tempfile.gettempdir(), str(uuid.uuid4())[-12:])
        os.makedirs(temp_unzip_dir)
        base_zip = zipfile.ZipFile(os.path.join(os.getcwd(), self.basezip))
        base_zip.extractall(path=temp_unzip_dir)
        logger.info("Extracted CTFd config zip")

        # Merge dirs
        config_files_to_copy = [f for f in os.listdir(os.path.join(temp_unzip_dir, 'db')) if
                                f not in os.listdir(tempdbdir)]
        for f in config_files_to_copy:
            shutil.copy2(os.path.join(temp_unzip_dir, "db", f), os.path.join(tempdbdir, f))
        shutil.rmtree(temp_unzip_dir)
        logger.info("Merged CTFd config zip with generated files")

        output_zip_name = os.path.join(os.getcwd(), "output", f"{self.event.name}.ctfd.{time.strftime('%Y.%m.%d-%H.%M.%S')}.zip")
        ctfd_dir = pathlib.Path(tempdirname) / 'ctfd'
        ctfd_dir_prefix = len(ctfd_dir.as_posix()) + 1
        with ZipFile(output_zip_name, "w") as buildzip:
            buildzip.write(ctfd_dir / 'db', arcname='db')
            for root, dirs, files in os.walk(ctfd_dir):
                for file in files:
                    buildzip.write(
                        os.path.join(root, file),
                        arcname=os.path.join(root, file)[ctfd_dir_prefix:]
                    )
        logger.info(f"Created CTFd upload zip as {output_zip_name}")

    def make_output_folder(self):
        """ Builds a temporary output folder to copy challenge data into before zip
        returns
        -------
        tuple of:
            - tempdirname   - the path to the top of the dir
            - tempuploaddir - the path to the upload folder, where challenge files go
            - tempdbdir     - the path to the db dir, where json files go
        """
        tempuploaddir = f"{self.event.out_dir}/ctfd/uploads"
        tempdbdir = f"{self.event.out_dir}/ctfd/db"
        
        os.makedirs(tempuploaddir)
        os.makedirs(tempdbdir)
        return self.event.out_dir, tempuploaddir, tempdbdir


# literally just a token class for challenge to abuse
# TODO: there is probably a way around using this using dictionaries
class _chal_rep(object):
    def __init__(self):
        return


def dump_to_json(objects):
    return {
        'count': len(objects),
        'results': [o.__dict__ for o in objects],
        'meta': {}
    }


def get_ctfd_flag_list(challenges: List[Challenge]):
    """Builds a list of objects from challenges that dump nicely into json for CTFd """
    challenge_flag_list = []
    for chal in challenges:
        challenge_flag_list += ctfd_flag_repr(chal)
    challenge_flag_list = [flag for flag in challenge_flag_list if flag is not None]
    for i in range(len(challenge_flag_list)):
        challenge_flag_list[i].id = i + 1
    return challenge_flag_list


def copy_zip_file_to_temp(chal, tempdir):
    # not provided for chal
    if not chal.has_challenge_zip:
        return
    filename = "_".join(chal.name.split(" "))
    os.makedirs(os.path.join(tempdir, filename))
    for file in chal.files:
        # missing
        if not os.path.exists(file):
            raise RuntimeError(f"{chal.name}: player files at {file} does not exist!")

        shutil.copy2(
            file,
            os.path.join(
                tempdir,
                f"{filename}/{os.path.basename(file)}"
            )
        )
    return


def ctfd_file_list(chal):
    retVals = []
    filename = "_".join(chal.name.split(" "))
    for file in chal.files:
        # Build obj
        retVal = _chal_rep()
        retVal.id = None
        retVal.type = "challenge"
        retVal.location = f"{filename}/{os.path.basename(file)}"
        retVal.challenge_id = chal.id
        retVal.page_id = None
        retVals.append(retVal)
    return retVals


def ctfd_repr(chal):
    """challenges.json representation for CTFd db"""
    retVal = _chal_rep()
    retVal.id = chal.id
    retVal.name = chal.name
    retVal.description = chal.description
    retVal.connection_info = chal.connection_info
    retVal.next_id = chal.next_id
    retVal.max_attempts = chal.max_attempts
    retVal.value = chal.value
    retVal.category = chal.category
    retVal.type = chal.type
    retVal.state = chal.state
    retVal.requirements = chal.requirements
    retVal.attribution = chal.attribution
    return retVal


def ctfd_flag_repr(chal):
    retVals = []
    for flag in chal.flags:
        retVal = _chal_rep()
        # These are as specified in CTFd's challenge json, do not edit
        retVal.id = None                # to be set later
        retVal.challenge_id = chal.id   # The associated challenge id
        retVal.type = "static"          # "static" or whatever the value for regular expression is
        retVal.content = flag           # the flag string or regex
        retVal.data = None if chal.case_sensitive else "case_insensitive"  # ??? maybe something with regex
        retVals.append(retVal)
    return retVals
