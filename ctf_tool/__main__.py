#!/usr/bin/env python3
"""Django's command-line utility for administrative tasks."""
import os
import sys
import argparse
import logging

def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ctf_tool.settings.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    # Set logging level at global hook because django somehow doesn't do this
    # parser = argparse.ArgumentParser(add_help=False)
    # parser.add_argument("-v","--verbose",dest="level",type=int,choices=range(0,4),default=0)
    # args, _ = parser.parse_known_args()
    # logging.getLogger().setLevel(10*(4-args.level))

    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
