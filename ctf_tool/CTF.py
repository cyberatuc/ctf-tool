import logging
import time
import os
from enum import Enum

from ctf_tool.challenge import get_challenge_list
from ctf_tool.CTFd import CTFdScoreboard
from ctf_tool.docker import DockerComposeInfrastructure

logger = logging.getLogger(__file__)


class CTF():
    """ Container for constructing a CTF """
    class RunLevel(Enum):
        PARSE_CHALLENGES = 1
        PRE_BUILD_VALIDATION = 2
        BUILD = 3
        CHECK = 4
        POST_BUILD_VALIDATION = 5
        INFRA_GENERATION = 6
        SCOREBOARD_GENERATION = 7
        ALL = 99

    def __init__(self,
                 *,
                 name=None,
                 address=None,
                 slug=None,
                 challenge_dirs=None) -> None:
        self.name = name or 'MyCTF'
        self.address = address or 'localhost'
        self.slug = slug or 'ctf'
        self.challenges = []
        self.challenge_dirs = challenge_dirs
        self.scoreboards = []
        self.infrastructure = []
        self.errors = []
        self.out_dir = os.path.join("output", self.name + time.strftime(".%Y.%m.%d-%H.%M.%S"))

    def run(self, until=RunLevel.ALL):
        logger.debug("Pre-Parsing challenges")
        self.challenges = get_challenge_list(self.challenge_dirs, self)
        logger.debug("Pre-Parsing done")
        if until == CTF.RunLevel.PARSE_CHALLENGES:
            return

        logger.debug("Pre-validating challenges")
        self.pre_build_validate()
        logger.debug("Pre-validating done")
        if until == CTF.RunLevel.PRE_BUILD_VALIDATION:
            return

        logger.debug("building challenges")
        self.build(clean_first=False)
        logger.debug("building done")
        if until == CTF.RunLevel.BUILD:
            return

        self.check()
        if until == CTF.RunLevel.CHECK:
            return

        self.post_build_validate()
        if until == CTF.RunLevel.POST_BUILD_VALIDATION:
            return

        self.generate_infrastructure()
        if until == CTF.RunLevel.INFRA_GENERATION:
            return

        self.generate_scoreboard()
        if until == CTF.RunLevel.SCOREBOARD_GENERATION:
            return

    def pre_build_validate(self):
        for chal in self.challenges:
            if not chal.pre_build_validate():
                self.errors.append(chal)

    def clean(self):
        for chal in self.challenges:
            chal.clean()

    def build(self, clean_first=True):
        for chal in self.challenges:
            chal.build()

    def check(self):
        for chal in self.challenges:
            chal.check()
        logger.debug("TODO: check methods")
        pass

    def post_build_validate(self):
        for chal in self.challenges:
            if not chal.post_build_validate():
                self.errors.append(chal)

    def generate_infrastructure(self):
        for infra in self.infrastructure:
            infra.generate()

    def generate_scoreboard(self):
        for scoreboard in self.scoreboards:
            scoreboard.generate()
