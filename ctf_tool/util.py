import os
import shutil
from pathlib import Path

class EmptyConfigFileError(Exception):
    pass


def contents_of(file):
    """Effectively open(file).read() but inline and slightly nicer"""
    if Path(file).exists():
        return open(file).read().strip()


def copytree(src, dst, symlinks=False, ignore=None):
    """
    Handles the root dir already existing
    https://stackoverflow.com/questions/1868714/how-do-i-copy-an-entire-directory-of-files-into-an-existing-directory-using-pyth
    """
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


def get_resource_file_path(filename):
    """Get absolute path to a file in the resource dir"""
    return os.path.abspath(os.path.dirname(__file__) + "/resources/" + filename)
