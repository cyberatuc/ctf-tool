"""
TOR network support for challenges.
"""
from jinja2 import Template

tor_service_template = """
#
# ctf-tool generated torrc for {{ctf_name}}
# 
# If individual onion addrs desired, enable the HiddenServiceDir's above each port
# 
# If you generated the ctf without the tor container, add this as a compose service:
# tor:
#     image: thetorproject/obfs4-bridge
#     command: tor
#     volumes:
#     - ./torrc:/etc/tor/torrc
#     - ./tor-data:/var/lib/tor


HiddenServiceDir /var/lib/tor/{{ctf_name}}/
{% for challenge in challenge_list %}
#HiddenServiceDir /var/lib/tor/{{ctf_name}}.{{challenge.username}}/
HiddenServicePort {{challenge.port}} 172.17.0.1:{{challenge.port}}
{% endfor %}
"""


tor_compose_container = {
    'image': 'thetorproject/obfs4-bridge',
    'command': 'tor',
    'volumes': [
        './torrc:/etc/tor/torrc',
        './tor-data:/var/lib/tor',
    ]
}


def generate_torrc(ctf_name, challenge_list):
    """ Generate a torrc for exposing challenges as onion services """
    template = Template(tor_service_template)
    return template.render({
        'ctf_name': ctf_name,
        'challenge_list': challenge_list,
    })
