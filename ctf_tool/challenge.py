import os
import random
import re
import shlex
from subprocess import Popen
from typing import List, Any
from uuid import uuid4
import logging
from pathlib import Path
from enum import Enum

from jinja2 import Template
import yaml

from ctf_tool.util import (EmptyConfigFileError, contents_of,
                           get_resource_file_path)


logger = logging.getLogger(__file__)


def print_invalid_challenge_message(dir):
    header = f"INVALID CHALLENGE DIRECTORY: {dir}"
    print("="*len(header))
    print(header)
    print("-" * len(header))
    print("You are seeing this message because the challenge directory above does not have valid metadata.")
    print("Go to this directory and create a challenge.yaml file or remove this directory from the list of targets.")
    print("="*len(header))


class Challenge(object):
    class Version(Enum):
        LEGACY = 0 # Legacy dir of text files
        YAML_V1 = 1 # first yaml version

    def __init__(self, abs_directory, event):
        # Local fs info
        self.event = event
        self.directory = abs_directory
        self.category = os.path.basename(os.path.dirname(self.directory))
        self.tool_version = None
        self._yaml_data = None

        # Standard Challenge
        self.id = None
        self.name = None
        self.flag_list = None
        self.flags = []
        self.case_sensitive = True
        self.next_id = None # This points to the next recommended challenge for the user to solve; I don't ever plan on implementing this.
        self.description:str = None
        self.connection_hint:str = "nc {{event.address}} {{challenge.port}}"
        self.brief = None
        self.max_attempts = 0  # TODO: no demo yet, test and document
        self.value = None
        self.type = 'standard'
        self.state = 'visible'
        self.requirements = None
        self.attribution = None

        # Server Challenge
        self.requires_server_path = None
        self.server_zip_path = None
        self.username = None
        self.port = random.randint(30000, 50000)
        self.requires_server_string = None
        self.listener_command = None
        self.connection_info = None

        self.services = {}
        self.dockerfile = None
        self.docker_privileged = False
        self.build_cmd = None

        # Files
        self.has_challenge_zip = None # For legacy support
        self.files = []

    #
    # Directory Parsing
    #
    @staticmethod
    def from_directory(abs_directory, event) -> List[Any]:
        _yaml_path = Path(abs_directory) / 'challenge.yaml'
        if (_yaml_path).exists():
            data = yaml.safe_load(open(str(_yaml_path)))
            return Challenge._from_yaml(abs_directory, event, data)
        else:
            return Challenge._from_legacy(abs_directory, event)

    @staticmethod
    def _from_yaml(abs_directory, event, data) -> List[Any]:
        results = []
        for name, chal_data in data['challenges'].items():
            chal = Challenge(abs_directory, event)
            chal.tool_version = Challenge.Version(int(data['version']))

            # Standard Challenge Meta
            chal.name = name
            chal.flag_list = chal_data.pop('flag')
            chal.case_sensitive = chal_data.pop('case_sensitive', True)
            chal.value = chal_data.pop('value')
            chal.brief = chal_data.pop('brief', None)
            chal.attribution = chal_data.pop('author', None)

            # Static ports
            random.seed(name)
            chal.port = random.randint(10000, 60000)

            # Player Facing
            chal.description = chal_data.pop('message')
            chal.has_challenge_zip = chal_data.pop('player_files', None)
            if chal.has_challenge_zip:
                for file in chal.has_challenge_zip.split(','):
                    chal.files.append(str(Path(chal.directory) / file.strip()))
            chal.connection_hint = chal_data.pop('connection_hint', chal.connection_hint)

            # Server Challenge (legacy)
            chal.username = force_valid_username(chal.name)
            chal.server_zip_path = chal_data.pop('server_files', chal.server_zip_path)
            chal.requires_server_string = chal_data.pop('server_command', chal.requires_server_string)
            chal.requires_server_path = bool(chal.requires_server_string) or None
            chal.listener_command = f"python3 /usr/local/bin/challenge-listener.py '{chal.requires_server_string}' {chal.port}"

            # Services (Docker)
            chal.dockerfile = chal_data.pop('dockerfile', None)
            chal.has_docker_build = bool(chal.dockerfile)
            chal.docker_privileged = chal_data.pop('privileged', chal.docker_privileged)
            chal.services = chal_data.pop('services', chal.services)
            if chal.dockerfile:
                chal.services['challenge'] = {
                    'build': {
                        'context': '.',
                        'dockerfile': chal.dockerfile
                    },
                    'privileged': chal.docker_privileged,
                    'tty': True,
                    'ports': [f'{chal.port}:{chal.port}'],
                }
            # normalize docker build tags to expanded format for ease of reference
            for service,config in chal.services.items():
                if not config.get('build', False):
                    continue
                chal.has_docker_build = True
                if type(config['build']) == str:
                    config['build'] = {
                        'context': config['build'],
                        'dockerfile': 'Dockerfile'
                    }

            # chal.build_cmd = chal_data.pop('build_cmd', None)# TODO: is this worth fixing?
            if (Path(chal.directory) / 'Makefile').exists():
                chal.build_cmd = 'make'

            # warning on remaining elements
            results.append(chal)
            for remaining in chal_data.keys():
                logger.warn(f"Unused YAML tag {remaining}")
        return results

    @staticmethod
    def _from_legacy(abs_directory, event) -> Any:
        chal = Challenge(abs_directory, event)
        chal.tool_version = Challenge.Version.LEGACY
        chal.name = os.path.basename(chal.directory)
        try:
            chal._parse_legacy_options()
        except AttributeError:
            print_invalid_challenge_message(abs_directory)
            return []
        return [chal]

    def _parse_legacy_options(self):
        # Metadata
        self.flags = [contents_of(os.path.join(self.directory, "flag.txt"))]
        self.description = contents_of(os.path.join(self.directory, 'message.txt')).strip()
        self.max_attempts = int(contents_of(os.path.join(self.directory, "max-attempts")) if os.path.exists(os.path.join(self.directory, "max-attempts")) else 0)  # TODO: no demo yet
        self.value = int(contents_of(os.path.join(self.directory, 'value.txt')))
        _attribution = Path(self.directory) / 'attribution'
        if _attribution.exists():
            self.attribution = contents_of(_attribution)
        if (Path(self.directory) / 'Makefile').exists():
            self.build_cmd = 'make'

        # Server Challenge
        if (Path(self.directory) / 'requires-server').exists():
            self.requires_server_path = str(Path(self.directory) / 'requires-server')
        self.username = force_valid_username(self.name)
        if self.requires_server_path is not None:
            self.server_zip_path = os.path.join(os.path.split(self.requires_server_path)[0], "server.zip")
            self.set_requires_server_string()
            self.listener_command = f"python3 /usr/local/bin/challenge-listener.py '{self.requires_server_string}' {self.port}"
        _path = Path(self.directory) / 'Dockerfile'
        if _path.exists():
            self.dockerfile = 'Dockerfile'

        # Files
        if (Path(self.directory) / 'challenge.zip').exists():
            self.has_challenge_zip = str(Path(self.directory) / 'challenge.zip')
            self.files.append(self.has_challenge_zip)


    #
    # Build pipeline
    #
    def pre_build_validate(self) -> bool:
        is_valid = True
        if self.tool_version == Challenge.Version.LEGACY:
            logger.warn(f"{self.name} challenge is in legacy format, consider upgrading to YAML")
            logger.warn(f"    in {self.directory}")

        if not self.attribution:
            logger.warn(f"{self.name}: no author attribution")

        if not self.brief:
            logger.warn(f"{self.name}: no challenge brief")

        if not self.build_cmd:
            is_valid = is_valid and self.post_build_validate()

        if self.category in ['pwn', 'web'] and not (self.has_docker_build):
            logger.warn(f"{self.name}: is a {self.category} challenge but provides no service")

        return is_valid

    def build(self, target=None):
        if not self.build_cmd:
            return
        target = target or ''
        include_dir = get_resource_file_path('')
        cmd = f'make -I {include_dir} -s -C "{self.directory}" {target}'
        _env = os.environ.copy()
        _env.update({
                "CTF_NAME": self.event.name,
                "CTF_ADDRESS": self.event.address,
                "CTF_SLUG": self.event.slug,
                "CTF_CHAL_NONCE": str(uuid4())[:8],
                "CTF_CHAL_NAME": self.name
        })
        result = Popen(
            shlex.split(cmd),
            env=_env,
        )
        result.communicate()
        assert result.returncode == 0, "Make failed to build challenges"

        if target == 'clean':
            return

        if self.tool_version == Challenge.Version.LEGACY:
            self._parse_legacy_options()
            return

    def clean(self):
        return self.build(target='clean')

    def check(self):
        # return self.build(target='check')
        return

    def post_build_validate(self) -> bool:
        is_valid = True
        if self.tool_version == Challenge.Version.LEGACY:
            pass
        elif self.tool_version == Challenge.Version.YAML_V1:
            pass
        # Common validation
        if self.value is None:
            logger.error("Challenge does not have a value!")
            is_valid = False
        if self.description is None:
            logger.error("Challenge does not have a message!")
            is_valid = False
        if self.flag_list is None and not self.flags:
            logger.error(f"{self.name}: Flag does not exist")
            is_valid = False
        if self.attribution is None:
            logger.warning("Challenge does not have attribution")
        # TODO: repair this
        # if self.requires_server_path or self.requires_server_string:
        #     if "server.zip" not in fileList:
        #         logger.error(f"{dirname}: server.zip missing when requires-server specified")
        #         is_valid = False
        #     elif open(f"{dirname}/requires-server").read().strip() not in zipfile.ZipFile(f"{dirname}/server.zip").namelist():
        #         logger.error(f"{dirname}: requires-server lists a binary not in server.zip")
        #         is_valid = False

        # YAML post-build option resolving
        if self.flag_list is not None:
            if self.flag_list.startswith("file:"): # For flag(s) in a file
                _path = Path(self.directory) / self.flag_list[5:]
                with open(_path) as fp:
                    flag_data = fp.read().strip() # Fix issue where newline gets included in flag
                    self.flags = [flag.strip() for flag in flag_data.splitlines()]
            else: # for inline flags
                self.flags = [flag.strip() for flag in self.flag_list.split(',')]
            
            for flag in self.flags:
                if (self.event.slug not in flag):
                    logger.warning(f"{self.name} doesn't seem to have a dynamic flag")

        if self.description is not None and self.description.startswith("file:"):
            _path = Path(self.directory) / self.description[5:]
            with open(_path) as fp:
                self.description = fp.read()

        if (self.services) and not (self.has_docker_build):
            logger.warning(f"{self.name} specified docker services but doesn't build any images!")

        return is_valid

    #
    # Utilities
    #
    def set_requires_server_string(self):
        with open(self.requires_server_path, "r") as f:
            requires_server_string = f.readline()

        if requires_server_string == "" or requires_server_string is None:
            raise EmptyConfigFileError
        requires_server_string = re.sub("(\r)*\n", "", requires_server_string)

        requires_server_args = requires_server_string.split(" ")
        if len(requires_server_args) > 1:
            requires_server_args[1] = os.path.join(f"/home/{self.username}", requires_server_args[1])
        else:
            requires_server_args[0] = os.path.join(f"/home/{self.username}", requires_server_args[0])
        requires_server_string = " ".join(requires_server_args)

        self.requires_server_string = requires_server_string

    def summarize(self, keys:List[str]):
        summary = {}
        for key in keys:
            summary[key] = getattr(self, key, None)
        return summary


def get_challenge_list(directory_list: List[str], event) -> List[Challenge]:
    """ Builds a list of challenge objects from all challenge bundles in args.directory """
    challenges = []
    challenge_names = []

    for challenge_pack in directory_list:
        problem_dir = os.path.join(os.getcwd(), challenge_pack)
        category_dirs = [dir for dir in os.listdir(problem_dir) if os.path.isdir(os.path.join(problem_dir, dir)) and not dir.startswith('.')]
        for category in category_dirs:
            for challenge in os.listdir(os.path.join(problem_dir, category)):
                if os.path.isdir(os.path.join(problem_dir, category, challenge)):
                    chals = Challenge.from_directory(os.path.join(problem_dir, category, challenge), event)
                    for chal in chals:
                        if chal.name in challenge_names:
                            logger.error(f"Two or more challenges named {chal.name}")
                            # TODO: v2 This is because of the zip file names for challenge.zip not being hash based. This may also be a limitation of CTFd, need to investigate
                            # quit(1)
                        challenge_names.append(chal.name)
                    challenges += chals
    for i in range(len(challenges)):
        challenges[i].id = i + 1
    return challenges


def force_valid_username(name):
    """force replace certain special characters with _, force to lowercase,
    truncate username if it is over 30 characters. Some people think they are super clever when
    they use accents in their challenge names. (╯°□°）╯︵ ┻━┻"""
    resulting_username = re.sub("[^A-Za-z0-9_-]", "_", name)
    if resulting_username is None:
        raise RuntimeError(f"Sanitized username for challenge {name} was garbage!")
    return resulting_username.lower()
