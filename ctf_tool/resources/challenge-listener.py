#!/usr/bin/python
import argparse
import os
import time

parser = argparse.ArgumentParser()
parser.add_argument("program", type=str)
parser.add_argument("port")
args = parser.parse_args()

while 1:
    os.system(f"socat TCP-LISTEN:{args.port},tcpwrap=script,reuseaddr,fork EXEC:'{args.program}',stderr,pty,rawer")
