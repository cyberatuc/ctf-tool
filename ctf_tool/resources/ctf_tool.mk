# default env vars
CTF_SLUG ?= flag
CTF_CHAL_NONCE ?= NONCE

# pattern rules for common targets
%.zip:
	zip $@ $^

.dynamic_flag:
	echo '$(CTF_SLUG){${FLAG_TEXT}_$(CTF_CHAL_NONCE)}' > flag.txt
