# TODO: I think setuptools_scm has a way to inject this back into the tool
version = 3.0

ascii_art = r"""
                                            __
                                _____....--' .'
                        ___...---'._ o      -`(
            ___...---'            \   .--.  `\
    ___...---'                      |   \   \ `|
    |                                |o o |  |  |
    |                                 \___'.-`.  '.
    |                                      |   `---'
    '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'

   mmm mmmmmmm mmmmmm       mmmmmmm  mmmm   mmmm  m
 m"   "   #    #               #    m"  "m m"  "m #
 #        #    #mmmmm          #    #    # #    # #
 #        #    #       "'"     #    #    # #    # #
  "mmm"   #    #               #     #mm#   #mm#  #mmmmm
"""
