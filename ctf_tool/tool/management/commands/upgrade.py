import argparse
import logging
from pathlib import Path
import os

from django.core.management.base import BaseCommand
import yaml

from ctf_tool.CTF import CTF
from ctf_tool.challenge import Challenge


logger = logging.getLogger(__file__)


class Command(BaseCommand):
    help = ('Upgrade legacy challenges to YAML')

    def add_arguments(self, parser):
        parser.fromfile_prefix_chars = '@'
        parser.add_argument("directory",
                            nargs="+",
                            help="Directories of challenge packs to load")
        parser.add_argument("--write",
                            default=False,
                            action="store_true",
                            help="Write new challenge.yaml to challenge dirs")
        return parser

    def handle(self, *argv, **args):
        event = CTF(
            name = 'validation_ctf',
            address = 'localhost',
            slug='TEST',
            challenge_dirs=args['directory']
        )
        event.run(CTF.RunLevel.PARSE_CHALLENGES)

        for chal in event.challenges:
            if chal.tool_version == Challenge.Version.LEGACY:
                _path = Path(chal.directory) / 'challenge.yaml'
                _player_files = None
                if chal.has_challenge_zip:
                    _player_files = os.path.basename(chal.has_challenge_zip)
                _server_files = None
                if chal.requires_server_string:
                    _server_files = os.path.basename(chal.requires_server_string)
                chal_data = {
                        'brief': '',
                        'author': chal.attribution,
                        'flag': 'file:flag.txt',
                        'value': chal.value,
                        'message': "file:message.txt",
                        'player_files': _player_files,
                        'server_files': _server_files,
                        'server_command': chal.requires_server_string
                }
                chal_data_clean = {}
                for key, val in chal_data.items():
                    if val is not None:
                        chal_data_clean[key] = val
                chal_data = {
                    'version': Challenge.Version.YAML_V1.value,
                    'challenges': {chal.name: chal_data_clean}
                }
                chal_data = yaml.safe_dump(chal_data)
                print(chal_data)
                if args['write']:
                    with open(_path, 'w') as fp:
                        fp.write(chal_data)

def get_parser():
    cmd = Command()
    parser = argparse.ArgumentParser(description=Command.help)
    cmd.add_arguments(parser)
    return parser
