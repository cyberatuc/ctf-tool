import argparse
import sys
import logging
import shutil
import atexit

from tabulate import tabulate
from django.core.management.base import BaseCommand

from ctf_tool.CTF import CTF


logger = logging.getLogger(__file__)


class Command(BaseCommand):
    help = ('Validate sets of problems')

    def add_arguments(self, parser):
        parser.fromfile_prefix_chars = '@'
        parser.add_argument("directory",
                            nargs="+",
                            help="Directories of challenge packs to load")
        parser.add_argument('--no-make', # TODO: broke
                            dest="no_make",
                            default=False,
                            action='store_true',
                            help="Don't run make for challenges with Makefiles")
        parser.add_argument("--clean",
                            default=False,
                            action='store_true',
                            help='Runs clean on chals')
        parser.add_argument("--summarize",
                            nargs='+',
                            default=['attribution', 'brief'],
                            help='challenge fields to summarize in the report')
        return parser

    def handle(self, *argv, **args):
        event = CTF(
            name = 'validation_ctf',
            address = 'localhost',
            slug='TEST',
            challenge_dirs=args['directory']
        )

        if args['clean']:
            atexit.register(event.clean)

        event.run(CTF.RunLevel.POST_BUILD_VALIDATION)
        has_errors = any(event.errors)

        # post-everything - print errors
        if has_errors:
            _errors = list(zip(args['directory'], event.errors))
            _errors.sort(reverse=True, key=lambda x:x[-1])
            print(tabulate(_errors, headers=['Challenge Pack', 'Errored Challenges']))
            exit(has_errors)

        # Output Reports
        challenges = event.challenges

        # total chal list
        width,_ = shutil.get_terminal_size((80,20))
        print('-' * width)
        _data = [chal.summarize(["category", "name", "value"] + args['summarize']) for chal in challenges]
        _data.sort(key=lambda x:x['name'])
        _data.sort(key=lambda x:int(x['value']))
        _data.sort(key=lambda x:x["category"])
        print(tabulate(_data, headers="keys", showindex='always'))
        print('-' * width)

        # Category histogram
        categories = [c.category for c in challenges]
        categories = list(set(categories))
        category_counts = [
            len(list(filter(lambda x:x.category == cat, challenges)))
            for cat in categories
        ]
        category_percentage = [
            ('X') * int(40 * count / len(challenges)) + ('-') * int(40 * (1-(count / len(challenges))))
            for count in category_counts
        ]
        categories_hist = list(zip(categories, category_counts, category_percentage))
        categories_hist.sort(key=lambda x:x[0])
        print(tabulate(categories_hist, headers=['category','problem count','percentage']))


def get_parser():
    cmd = Command()
    parser = argparse.ArgumentParser(description=Command.help)
    cmd.add_arguments(parser)
    return parser
