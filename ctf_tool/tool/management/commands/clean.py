import argparse
import sys
import logging
import shutil
import atexit

from tabulate import tabulate
from django.core.management.base import BaseCommand

from ctf_tool.CTF import CTF


logger = logging.getLogger(__file__)


class Command(BaseCommand):
    help = ('Clean sets of problems')

    def add_arguments(self, parser):
        parser.fromfile_prefix_chars = '@'
        parser.add_argument("directory",
                            nargs="+",
                            help="Directories of challenge packs to load")
        return parser

    def handle(self, *argv, **args):
        logger.info("cleaning...")
        event = CTF(
            name = 'validation_ctf',
            address = 'localhost',
            slug='TEST',
            challenge_dirs=args['directory']
        )
        event.run(CTF.RunLevel.PARSE_CHALLENGES)
        event.clean()


def get_parser():
    cmd = Command()
    parser = argparse.ArgumentParser(description=Command.help)
    cmd.add_arguments(parser)
    return parser
