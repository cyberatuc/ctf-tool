import argparse
import logging

from ctf_tool.CTF import CTF
from ctf_tool.CTFd import CTFdScoreboard
from ctf_tool.docker import DockerComposeInfrastructure
from ctf_tool.util import get_resource_file_path

from django.core.management.base import BaseCommand, CommandError

logger = logging.getLogger(__file__)


class Command(BaseCommand):
    help = ('Build a CTF')

    def add_arguments(self, parser):
        parser.fromfile_prefix_chars = '@'
        # Event meta
        parser.add_argument("--name",
                            default="ctf-tool",
                            help="Name of the output zip file")
        parser.add_argument("--address",
                            default='localhost',
                            help="Server address to list in CTFd for participants to connect to",
                            required=True)
        parser.add_argument("--flag-slug",
                            default='ctf',
                            help="Custom clag slug (ctf in ctf{flag}) to pass to builds")
        # Chals
        parser.add_argument("directory",
                            help="Directories of challenge packs to load",
                            nargs="+")
        # CTFd
        CTFdScoreboard.add_arguments(parser)
        # Docker
        DockerComposeInfrastructure.add_arguments(parser)
        # Chal build related
        parser.add_argument("--force",
                            action="store_true",
                            help="ignore challenge pack validation errors")
        parser.add_argument("--no-make",
                            action='store_true',
                            default=False,
                            help="Don't run `make` on challenges with Makefiles when building")
        parser.add_argument("--no-make-clean",
                            action='store_true',
                            default=False,
                            help="Don't run `make clean` but still use make")
        return parser

    def handle(self, *argv, **args):
        event = CTF(
            name = args['name'],
            address = args['address'],
            slug = args['flag_slug'],
            challenge_dirs=args['directory']
        )
        event.infrastructure.append(DockerComposeInfrastructure(event, args))
        event.scoreboards.append(CTFdScoreboard(event, args))

        event.run()

def get_parser():
    cmd = Command()
    parser = argparse.ArgumentParser(description=Command.help)
    cmd.add_arguments(parser)
    return parser
