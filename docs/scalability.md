# Architecture and Scalability Notes
## Current Implementation and Issues Therein
Currently the Architecture of ctf-tool's deployments are like so:
```
player facing side
|
|-------Scoreboard (CTFd)
|           |- Challenge front end website
|           |- Challenge Static Files (local disk+big = slow)
|           |- sqlitedb (also on disk = slow, not replicable)
|
|-------Challenge Host (Docker)
            |- Challenge 1 Container 
            |   |- in-container service            
            |- Challenge 2 Container
            |- ...
```

Issues By Component:
1. Scoreboard / CTFd
    - Single instance, no current replication system
    - Challenge Static Files are served from disk on the host, these could be off-loaded to cloud storage such as S3
    - database is a local sqlite file, which is slow and not scalable
2. Challenge Host
    - Currently, there is only a single host for the challenges, and challenges are run with a single container that forks connections (or is a web server)
    - There are Several opportunities to make this more scalable:
      - More than one challenge host, load balanced
      - More than one container per challenge (or just one container per team), furhter load balanced
      - Further resource limitations on the forking component that happens within the container on connection (ie global timeout)
    - Another important concept missing from ctf-tool at themoment is the differnece between global/stateless challenges and instanced challenges
      - Global/stateless is howt the system works now, where any number of players connect to the same target
        - this works in the general case, however it is not ideal as platers may impact the play of others either accidentally (general resource usage) or on purpose (rm -rf /*) which then requires manual interention
      - instanced challenges are those that are run for a single team or player, with a timeout on how long they are running vs how long the player may remain connected
        - instanced challenges also allowattributing misbehavior to specific players as well as deeper randomness such as per-player flags and topology simmulations for challenges such as arp spoof attacks without having to worry about other players causing issues

Additionally, there is currently NO performance monitoring in the infrastructure at any point, so beyond plaer complaint, there is no way to know a challenge is broken or overloaded.

## Proposed Design Changes
There are three main data paths that need to be considered for scalability:
1. The scoreboard, both for viewing challenge details and submitting flags
    - The scoreboard will need some basic webapp scalability principles applied to it
    - A (potentially also load balanced) reverse proxy such as nginx
    - The database needs to be moved to a real database saervice, and this needs to be accessible from all scoreborad replicas
      - At most, replicated read-only db's may also be needed here, although one postgres or mareiadb/mysqldb instance should be able to serve a very high number of connections
2. The static files, which are functionally distinc from the scoreboard in the sense that they are not dynamic content. Although they are currently hosted on the CTFd instance, the two are not necesarily tied together
    - First, they need to be separated from the scoreboard in terms of hosting, I think both ctfd and rctf support S3 providers, but this requires more research
    - A caching proxy in front may also make a considerable difference here, ideally none of these requests are made more than once per player and we can heavily lean on broswer caching
    - Since the players will still be going through the scoreboard, the scoreboard just changes it slinks from being on the same service to being on cloud storage, or we do that with a reverse proxy
3. The challenge containers
    - similar to the scoreboard, there needs to be a (potentially also replicated+load balanced) reverse proxy in front of the challenges
    - this proxy should then connect upstream to horizontally scalable challenge hosts (other approaches have taken this to mean auto-scaling k8s pods per challenge)
    - the challenge hosts then run the challenge containers, and autoscale individual problem containers as needed
    - This is where it gets different for stateless and instanced challenges
      - stateless - further optimizaitons to the current connect-fork method is fine, however replacing a simple fork with something like a fork+nsjail is probably more desirable so we can enforece connection timeouts and per-connection resource limits
      - instanced - a little more complicate and maybe for another day. Requires another service that can start other services,Other implementations (pwn.college dojo and DUCTF) use a service and a CTFd plugin that makes a server-server call so that this is never exposed to users. ALternatively, hackaat infra used a challenge cateway for ALL challenges that required a token to be given fo ryeach, however this required an entirely custom scoreboard that was able to hand out this tokens to teams

## The hidden tasks
1. for docker replication at scale, pushing all the base containers to a image registry that the challenge hosts can pull from is a must.
2. container load balancing and replication at scale really means k8s support, if we're going to make that conversion it needs to be done well, or we're just going to have 2x the issues that the current infra has
3. Scalable scoreboard configuration should be premade


# Future Service Gateway plans
We need a service gateway system to enable the following goals:
1. dynamically manage resource consumption
   1. only using what teams are connected to at any given time
2. provide user/team instanced challenges
   1. custom flags included
   2. entire networks can be spun up via netns trivially on connection
3. implement global challenge DOS prevention techniques, reducing the load on authors
   1. either connection timeout or 1:1 instance:team are common


misc TODO:
- write an entrypoint to check if flags are being dynamically built
- go clean the CTF repo makefiles, probably want to add an include here for *.zip and flag.txt, and probably more values for env vars

# Further Reading
- https://medium.com/@sam.calamos/how-to-run-a-ctf-that-survives-the-first-5-minutes-fded87d26d53
- https://github.com/google/nsjail
- https://rcds.redpwn.net/en/latest/challenge/