clean
-----
The clean command will attempt to clean challenge directories left dirty by
a previous build. This is really just a clean way to call ``make clean`` on
every challenge.

..  argparse::
    :module: ctf_tool.tool.management.commands.clean
    :func: get_parser
    :prog: ctf-tool clean
