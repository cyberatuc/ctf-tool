build
-----
Build is the main command for CTF tool, it takes in a list of challenge packs and outputs usable a environment.

Build will attempt to first run :ref:`Validate` by default.

..  argparse::
    :module: ctf_tool.tool.management.commands.build
    :func: get_parser
    :prog: ctf-tool build
