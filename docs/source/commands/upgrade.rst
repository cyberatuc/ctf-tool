upgrade
-------
The upgrade command is used to automatically update challenges from
the legacy format (directory of files per chal) to the new format (
yaml with potentially multiple challenges).

..  argparse::
    :module: ctf_tool.tool.management.commands.upgrade
    :func: get_parser
    :prog: ctf-tool upgrade
