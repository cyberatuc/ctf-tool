validate
--------
The validate command is used to validate sets of problems. This will allow a quick look at what issues may exist with a problem.

Input to the command is the top level dir of a challenge pack, multiple can be specified at once.

Exit code is 0 on valid, non zero on invalid.

..  argparse::
    :module: ctf_tool.tool.management.commands.validate
    :func: get_parser
    :prog: ctf-tool validate
