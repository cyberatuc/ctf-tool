Training Mode / Solutions
-------------------------
Not all CTFs have to be competitive events. If we can reuse problems without completely burning them, surely we can provide example solutions for at least some of the problems either after the event or in a training event.

While we have not made any specific technical decisions regarding a solution/training setting within ctf-tool, we do plan to implement one in the future so that we can bring at least basic problems to highschools in an educational event setting.
