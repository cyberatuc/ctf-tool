Problem Reuse
-------------
The largest time sync with any CTF is collecting/creating problems. Problems that have identical solutions/flags between events are considered burned.

Ideally, all problems can be writen in such a way that they can be in multiple events without having the solution or flag be static. The solutions we have discussed to this are as follows:

- problem semi-randomization
- flag randomization
- having enough problems that each event can select a subset of the problems that can in turn use the first two methods


ctf-tool currently has :ref:`Environment Variables Available` for accomplishing these goals:

- building from Makefiles (or dockerfiles) with some level of dev-specified randomness, such as randomization of buffer sizes in overflow problems via a python script called in the make file
- Environmental variables passed to ``make`` that allow the problem to be tailored to the current event and have a random slug for use as extra characters in a flag.


Other future ideas include:

- draw a flag from an external source such as reading from flag.txt or allow baking in a flag at build time if the flag is embedded in the problem
