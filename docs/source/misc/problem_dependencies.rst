Problem Dependencies
--------------------
Some problems, such as SQLi, require external resources such as a database. Typically these resources are just as ephemeral as the problems themselves, so it makes sense to be able to have container based dependencies for problems.

While not currently supported there is a future plan for the tool to allow per-challenge docker-compose environments. This method would also enable problems as networks of services/machines in the long term so we are interested in it for CDX situations.
