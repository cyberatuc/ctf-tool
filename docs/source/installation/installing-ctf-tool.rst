Installing ctf-tool
-------------------

From Gitlab
___________
Install using pip/git

    .. code-block:: sh

        pip3 install git+https://gitlab.com/cyberatuc/ctf-tool


From Cloned Respository
_______________________
Install using pip from the cloned respository

    .. code-block:: sh

        git clone git@gitlab.com:cyberatuc/ctf-tool
        pip3 install ctf-tool/


In Development Mode
___________________
The current preferred development environment is a basic python environment.

    .. code-block:: sh

        # Clone respository
        git clone git@gitlab.com:cyberatuc/ctf-tool

        # Setup dev venv, open a shell in context of venv
        pip3 install -U pip setuptools wheel
        pip3 install -Ue ./ctf-tool/[dev]
