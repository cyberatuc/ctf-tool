Building ctf-tool
-----------------
Building is completely optional when access to gitlab.com is available.


1. Update python packaging utilities

    .. code-block:: sh

        python3 -m pip install -U pip setuptools wheel

2. Build python source and binary distributions, these will be available in the dist directory.

    .. code-block:: sh

        python3 -m setup.py bdist_wheel sdist

3. (optional) Gather python dependencies

    .. code-block:: sh

        # this comand gathers wheel dependecies, which is preffered on glibc based systems
        python3 -m pip wheel . -w dist/

        # this command gathers source dependencies and may try to build them, not preffered
        python3 -m pip download . -d dist/ --no-binary :all:
