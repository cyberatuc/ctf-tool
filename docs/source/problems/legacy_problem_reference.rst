Legacy CTF Problem Configuration
--------------------------------
..  Important::

    ctf-tool version 3.0 and prefer the :ref:`challenge.yaml File Reference` style of problem configuration


Going from our abstract overview of a CTF problem, the minimal problem we can actually make consists of only the following:

    - a flag
    - communication to the player where/how they should access the problem
    - point value of the problem

Notice that the challenge is not included in this list, as a challenge could be externally hosted on other infrastructure outside of what ctf-tool manages.

Required Files
++++++++++++++

flag.txt
________

    Each problem must have a file called flag.txt. The contents of this file will be the flag that players must submit to the scoreboard to get points.

    Currently ctf-tool does not support regex style flags which CTFd does, so all players will submit the same flag which opens the opportunity for players to cheat by sharing flags.

    Example flag.txt:
        .. literalinclude:: ../../../challenges/test-challenge-pack/Examples/Standard Challenge/flag.txt


message.txt
___________

    Each problem must have a file called message.txt. The contents of message.txt will be presented to the player via the scoreboard.

    Example message.txt:
        .. literalinclude:: ../../../challenges/test-challenge-pack/Examples/Standard Challenge/message.txt


value.txt
_________

    Each problem must have a file called value.txt. The contents of value.txt must be an integer value and will be used for the score of the problem.

    There's no simple way to decide a score for a problem, although CTFd does allow a setting for dynamic weighting of problem scores.

    Example value.txt:
        .. literalinclude:: ../../../challenges/test-challenge-pack/Examples/Standard Challenge/value.txt


attribution
___________

    Each problem should have a file called attribution. This file should contain a one-line summary of the origin or author of the problem.

Presenting Files to Players
+++++++++++++++++++++++++++
Some challenges require the players to have access to some files, such as when presenting a reverse engineering problem.

ctf-tool takes a simple approach to this, any challenge that contains a :code:`challenge.zip` file will have this file presented to the user via CTFd.

This works well with the Makefile method described in :ref:`Dynamically Built Problems` to reduce the amount of duplicate files stored.


Dynamically Built Problems
++++++++++++++++++++++++++
Many times, static files alone are not good for representing a problem. Problems that build binaries don't usually need to have binaries stored in source control, parameters on problems can change, and adjusting difficulty are all examples of things that can be done with Makefiles.

To have a problem build when :code:`ctf-tool build` is run, simply include a Makefile.

When building or validating, ctf-tool will run the equivalent of :code:`make clean; make;` on each problem directory that includes a Makefile


Example Makefile
________________

    .. literalinclude:: ../../../challenges/test-challenge-pack/Examples/Standard Challenge with Makefile/Makefile


Environment Variables Available
_______________________________

    There are future plans to pass environmental variables via the call to make.
    If you're in the Cyber@UC gitlab group, the ctf problem oober-powered uses these in the make file.

    CTF_NAME -- the name of the ctf as specified

    CTF_ADDRESS -- address of the challenge server as specified

    CTF_SLUG -- a slug to wrap challenge flags in if possible, ie "myCtf" for "myCtf{flag}"

    CTF_CHAL_NONCE -- a random string of 8 characters for a challenge to use as a seed or extra random characters after a flag


Custom Docker Containers
________________________

    Custom docker containers may be used with challenges. The way this works is that ctf-tool will first invoke ``make`` with no target if a Makefile exists. If the ``--install-docker`` flag is used in conjunction, then a ``Dockerfile`` entry in the challenge dir will be rendered as a jinja template with access to its own challenge object. This Dockerfile is then free to use multi-stage builds, call back into make or another build system, or any other number of tricks to ensure the underlying challenge environment is as-expected.

    An example of this is given in ``challenges/test-challenge-pack/Example/Custom Dockerfile/`` in the git repo. Additionally, the challenge object is a quite large object, denoting the state of how ctf-tool has initialized and configured the challenge for the specific build, reading through ``Challenge.py`` will provide the best insight to what can be used from this in the docker file at jinja render time.


Hosting Challenge Services
++++++++++++++++++++++++++
..  Important::
    ctf-tool assumes all services will be running on a single docker environment/host over multiple exposed ports.

..  TODO: note the port range

Many times, problems require being run as a service that players can connect to. This can also be used to run the challenge in an asymmetric environment than what the player may download from challenge.zip, such as one that has flag.txt in it.

In the current state, ctf-tool will attempt to run specified commands/files inside docker containers. 

For any challenge that has a service, the string :code:`nc address port` will be added to the end of the challenge description in CTFd with the address specified to :code:`ctf-tool build` and the port randomly chosen for the service.


Specifying a Command to Run
___________________________

    All services require an entrypoint. Typically this is the name of your executable. If your executable is not native, it should include a shebang line.

    The entrypoint command should be in a file named :code:`requires-server` in the root of the challenge dir.


Server Files
____________

    If a service needs additional files such as compiled binaries, these can be passed via :code:`server.zip`. Server.zip should contain everything needed to run the problem, including the flag.

    To be used, server.zip must be present in the root of the challenge after make has run.


Custom Dockerfile
_________________

    A custom Dockerfile can be used for a problem and ctf-tool will automatically detect this if the Dockerfile is in the root of the challenge directory.

    Custom Dockerfiles are responsible for consuming artifacts such as server.zip

    If a Dockerfile is not specified, ctf-tool will use the following Dockerfile, templated with the challenge information. This Dockerfile is acceptable for basic x86 / x86_64 glibc binaries.

    ..  literalinclude:: ../../../ctf_tool/resources/Dockerfile
        :language: dockerfile
