Overview of Problems
--------------------
Generically, a CTF problem is a challenge that a player can prove completion of by submitting a flag.

This means that most CTF problems will contain some form of the following:

    - a problem to solve
    - a flag that the problem yields when solved
    - some way of communicating to the player where/how they should access the problem service
    - a service that hosts the problem (infrastructure)
    - somewhere the player can submit the flag for points (scoreboard)

ctf-tool focuses on the management and deployment of these components, while
remaining flexible enough for many types of CTF problems.

For a more basic introduction to CTF as a whole, https://trailofbits.github.io/ctf/ is a great resource.
