Directory Structure For Problems
--------------------------------
ctf-tool has a specific idea of how problems should be organized, this is mainly a shortcut to how the problems will be categorized in CTFd.

This directory structure is a top level folder, with the name of the challenge set, sub folders for each category of problem, and sub-subfolders for each challenge in the category.

.. code-block::

    <ctf_problem_set>/
        <category>/
            <problem_name>/
                challenges.yaml
                Makefile

It is also worth noting that soft links work in this structure.


..  TODO: move this into other files / notes on other stuff
    # Problems as networks / Problem Dependencies
    docker-compose.yml - (unimplemented)

    # Additional CTFd options
    max-attempts    - (optional) a single number that describes how many attempts users are allowed per. 0 = inf
    hint_<x>.txt    - (unimplemented) Hint file where x is the order the hints should be given

    # Training Modes
    solution.txt    - (unimplemented) details on how to solve the challenge
