.. Index

**********************
ctf-tool Documentation
**********************
A tool for building modular CTFs with minimal effort. Challenges
are configured with a metadata file that tells CTF tool if they
need to build with ``make`` or need a docker container to run in.
CTF tool then takes all artifacts and packages them for a scoreboard
or prepares a backend infrastructure like docker-compose.


..  Important::
    ctf-tool has been tested with CTFd versions up to 3.7.6. 

    To run CTFd at this version, please see the `Docker script <https://gitlab.com/cyberatuc/ctf-tool/-/blob/master/scripts/ctfd-docker.sh>`_


..  code-block:: sh

    pip3 install git+https://gitlab.com/cyberatuc/ctf-tool


..  toctree::
    :maxdepth: 2
    :caption: Building / Installation:

    installation/installing-ctf-tool
    installation/building-ctf-tool


..  toctree::
    :caption: ctf-tool Commands

    commands/validate
    commands/build
    commands/clean
    commands/upgrade


..  toctree::
    :caption: Creating Problems

    problems/overview
    problems/problem_dir_structure
    problems/challenge.yaml_reference
    problems/legacy_problem_reference


..  toctree::
    :maxdepth: 2
    :caption: User Guides:

    guides/setup_checklist
    guides/example_ctf_build
    guides/example_ctfd
    guides/example_dockerenv
    guides/example_live_updates
    guides/use_with_tor


..  toctree::
    :maxdepth: 2
    :caption: Additional Information

    misc/default_ctf_zip
    misc/problem_reuse
    misc/problem_dependencies
    misc/training_mode
