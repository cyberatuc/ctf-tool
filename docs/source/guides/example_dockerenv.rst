Deploying the Docker Services
-----------------------------
This covers the process of importing a build into CTFd, the files referenced were generated in :ref:`the example ctf<Building a CTF>`.

1. (optional) Copy the dockerenv from :code:`output/MyCTF.<date>/dockerenv/` to the hosting server. You may want to zip this first.

2. Install docker, the instructions for this are at https://docs.docker.com/get-docker/

3. Install docker-compose, the python distribution seems to be the most distro-agnostic version for now.

    ..  code-block:: sh

        pip3 install docker-compose

4. From inside the dockerenv folder, run the following commands to build and start the services

    ..  code-block:: sh

        docker-compose build
        docker-compose up -d

5. Check the status of the services to make sure everything is 'Up'

    ..  code-block:: sh

        docker-compose ps

6. When the CTF is finished, stop the servies:

..  code-block:: sh

        docker-compose down
