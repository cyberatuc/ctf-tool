Building a CTF
--------------
Provided with ctf-tool are three example CTF packs:

- test-challenge-pack, a tool smoke test
- over-the-wire-bandit, all the flags from OTW's Bandit
- over-the-wire-krypton, most of the flags from OTW's Krypton


This example will walk through deploying the test-challenge-pack, which includes some example problems and services.


1. Validate the pack, this ensures that ctf-tool has enough data for each problem to properly setup.0

    ..  code-block:: sh

        ctf-tool validate challenges/test-challenge-pack/


2. Build the CTF, in this example the problems will be hosted on ctf.example.com

    ..  code-block:: sh

        ctf-tool build --name MyCTF --address ctf.example.com --install-docker challenges/test-challenge-pack/
