Deploying to CTFd
-----------------
This covers the process of importing a build into CTFd, the files referenced were generated in :ref:`the example ctf<Building a CTF>`.

1. Start a CTFd instance, this can be done with the following docker command:

    ..  literalinclude:: ../../../scripts/ctfd-docker.sh


2. Browse to the CTFd instance and create an initial user. Note that this user and any settings configured will be overw/ritten after the build is uploaded.

3. Browse to Admin Panel > Config > Backup > Import

4. Upload the file :code:`output/MyCTF.ctfd.<date>.zip` and import.
