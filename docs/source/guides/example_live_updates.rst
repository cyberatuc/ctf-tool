Updating Running Services
-------------------------
This covers the process of importing a build into CTFd, the files referenced were generated in :ref:`the example ctf<Building a CTF>`.

Many things can go wrong during a CTF, and sometimes services need to be rebuild. With docker-compose this is easy.

1. Make edits to the problems that need modified

2. Rebuild the containers

    ..  code-block:: sh

        docker-compose build

3. Update the running services

    .. code-block:: sh

        docker-compose up -d
