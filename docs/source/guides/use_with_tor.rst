Using with TOR
--------------
ctf-tool supports exposing the challenge services over TOR for those times when you really don't want to use a cloud node.

A ``torrc`` file is always generated on build. This file can be used with tor at any time by starting a tor instance with the command ``tor -f output/ctf/torrc``. If you have already generated a TOR hidden service, this will allow you to re-use the address in the initial build command as the ctf address.

The ``--tor`` flag on build will enable a docker container that runs the torrc as generated alongside the challenges. To use a onion address at build time for this mode requires pre-building an onion service that the container can use. This can be done by loading the torrc on the host machine, copying it into tor-data, then changing the permissions on the copied dir to match the tor user in the container via docker-compose run.
