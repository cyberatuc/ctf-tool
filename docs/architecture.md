# Architecture of ctf-tool
It's basically a compiler, so it makes sense to organize it like one

```
Challenge Building (think compiling in the clang sense)
    - parsing challenge dirs
    - pre_build_validation
    - build
    - check - not implemented globally
    - post_build_validation
Follow on modules
    - infrastructure modules
    - scoreboard modules
```


```
What's in a CTF?

CTF
    Metadata
        name
    Category[]
        Challenge[]
    deployment configurations
        docker-compose
        other future...
    scoreboard configurations
        ctfd

1. CTF is instantiated from metadata
    ie the build command
2. CTF's challenges are parsed, categories are implicitly built from
    This is the opportunity to have multiple different challenge parsers so
    we can move away from the platter of files while not havnig to retool everything
3. Chals are built by builders
    - make - the most generic
    - docker - the most portable
4. Scoreboard data is built from the chals
    - ctfd
    - other scoreboards
5. Deployment Configurations are built from resulting challenge data/files
    - docker-compose environment
        - tor
    - aws ECS
    - kubernetes
    - other cloud/host types
```
