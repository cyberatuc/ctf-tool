#
# Build / Development Image
#
FROM python:3.13.1-slim-bookworm AS builder

ENV SHELL /bin/bash

RUN apt update && apt install -y \
        make \
        gcc \
        zip \
        git \
 && pip3 install -U pip setuptools wheel \
 && mkdir -p /ctf-tool/
WORKDIR /ctf-tool/
COPY . /ctf-tool/
RUN pip3 install .[dev]
CMD ctf-tool
