#!/usr/bin/env python3
from setuptools import setup, find_packages
from ctf_tool import version

setup(
    # Boilerplate
    name = "ctf-tool",
    version = version, # TODO: check setuptools_scm
    # Code
    packages = find_packages(),
    entry_points = {
        'console_scripts': [
            'ctf-tool=ctf_tool.__main__:main'
        ],
    },
    install_requires = [
        "django",
        "rich",
        "jinja2",
        "pyyaml",
        "tabulate",
    ],
    extras_require = {
        'dev': [
            "vulture",
            "isort",
            "pycodestyle",
            "sphinx-argparse",
            "sphinx",
            "sphinx-rtd-theme",
        ]
    },
    include_package_data=True, # Non code files
    # Meta
    author = "Cyber@UC",
    author_email = "cyberatuc513@gmail.com",
    url = "gitlab.com/cyberatuc/ctf-tool",
    description = "Tool for making and deploying CTF events",
    classifiers = [
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
    ]
)
